let config;

const queue = [];
let isRunning = false;

const mainContainer = document.getElementById('main-container');
const bonkee = document.getElementById('bonkee');
const bat = document.getElementById('bat');
const sound = document.getElementById('sound');

const run = (args) => {
    return new Promise((resolve) => {
        fetch(`https://decapi.me/twitch/avatar/${args}`).then((data) => {
            data.text().then((img) => {

                // Here we have the image and can start the animation
                bonkee.src = img;
                mainContainer.className = 'main-container';

                // After 1 second, start the animation and the sound
                setTimeout(() => {

                    sound.play();
                    bat.className = 'bat animate';
                    bonkee.className = 'bonkee animate';

                    // After 1 second clear the animation
                    setTimeout(() => {

                        mainContainer.className = 'main-container hidden';
                        bat.className = 'bat';
                        bonkee.className = 'bonkee';
                        resolve();

                    }, 980);

                }, 1000);

            }).catch(() => {
                resolve();
            });
        }).catch(() => {
            resolve();
        });
    });

};

const runQueue = async () => {
    if (isRunning) return;
    isRunning = true;

    while (queue.length > 0) {
        await run(queue.shift());
    }
    isRunning = false;
};

const checkPrivileges = (data) => {
    const required = config.privileges;

    const { tags, userId } = data;
    const { mod, subscriber, badges } = tags;

    const isMod = parseInt(mod);
    const isSub = parseInt(subscriber);
    const isVip = (badges.indexOf("vip") !== -1);
    const isBroadcaster = (userId === tags['room-id']);

    if (isBroadcaster) return true;
    if (required === "mods" && isMod) return true;
    if (required === "vips" && (isMod || isVip)) return true;
    if (required === "subs" && (isMod || isVip || isSub)) return true;
    return required === "everybody";
};

const handleMessage = (obj) => {
    const msgEventData = obj.detail.event.data;
    const msg = msgEventData.text.toLowerCase();

    if (!msg.startsWith(config.cmd) || !checkPrivileges(msgEventData)) {
        return;
    }

    const args = msg.replace(config.cmd, '').trim();
    queue.push(args);
    runQueue();
};

// See https://dev.streamelements.com/docs/widgets/6707a030af0b9-custom-widget-events#on-event
window.addEventListener('onEventReceived', function (obj) {
    if (obj.detail.listener !== "message") return;
    handleMessage(obj);
});

// See https://dev.streamelements.com/docs/widgets/6707a030af0b9-custom-widget-events#on-widget-load
window.addEventListener('onWidgetLoad', function (obj) {
    config = obj.detail.fieldData;
});