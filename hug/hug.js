let config;

const queue = [];
let isRunning = false;

const mainContainer = document.getElementById('mainContainer');
const hugger = document.getElementById('hugger');
const huggerHat = document.getElementById('huggerHat');
const huggerHeart = document.getElementById('huggerHeart');
const huggee = document.getElementById('huggee');
const huggeeHat = document.getElementById('huggeeHat');
const huggeeHeart = document.getElementById('huggeeHeart');

async function run(obj) {
    return new Promise(async (resolve) => {

        const christmasHatSrc = "https://gitlab.com/Knakra/streamelementswidgets/-/raw/main/assets/christmas_hat.png";
        if(config.christmasmode === "yes") {
            huggerHat.src = christmasHatSrc;
            huggeeHat.src = christmasHatSrc;
        }

        const huggersrc = await getAvatarSrc(obj.user);
        const huggeesrc = await getAvatarSrc(obj.args);

        hugger.src = huggersrc;
        huggee.src = huggeesrc;

        mainContainer.classList.toggle("hidden");

        await sleep(1000);

        hugger.className = "hugger animate";
        huggee.className = "huggee animate";
        huggerHat.className = "hugger hat animate";
        huggeeHat.className = "huggee hat animate";
        huggerHeart.className = "hugger heart animate";
        huggeeHeart.className = "huggee heart animate";

        await sleep(3000);

        mainContainer.classList.toggle("hidden");
        hugger.src = "";
        huggee.src = "";
        huggerHat.src = "";
        huggeeHat.src = "";
        hugger.className = "hugger";
        huggee.className = "huggee";
        huggerHat.className = "hugger hat";
        huggeeHat.className = "huggee hat";
        huggerHeart.className = "hugger heart";
        huggeeHeart.className = "huggee heart";

        resolve();
    });
};

function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

async function getAvatarSrc(user) {
    return new Promise((resolve) => {
        fetch(`https://decapi.me/twitch/avatar/${user}`).then((data) => {
            data.text().then((avatarSrc) => {
                resolve(avatarSrc);
            }).catch(() => { resolve(); });
        }).catch(() => { resolve(); });
    });
}

async function runQueue() {
    if (isRunning) return;
    isRunning = true;

    while (queue.length > 0) {
        await run(queue.shift());
    }
    isRunning = false;
};

function checkPrivileges(data) {
    const required = config.privileges;

    const { tags, userId } = data;
    const { mod, subscriber, badges } = tags;

    const isMod = parseInt(mod);
    const isSub = parseInt(subscriber);
    const isVip = (badges.indexOf("vip") !== -1);
    const isBroadcaster = (userId === tags['room-id']);

    if (isBroadcaster) return true;
    if (required === "mods" && isMod) return true;
    if (required === "vips" && (isMod || isVip)) return true;
    if (required === "subs" && (isMod || isVip || isSub)) return true;
    return required === "everybody";
};

function handleMessage(obj) {
    const msgEventData = obj.detail.event.data;
    const msg = msgEventData.text.toLowerCase();

    if (!msg.startsWith(config.cmd) || !checkPrivileges(msgEventData)) {
        return;
    }

    const args = msg.replace(config.cmd, '').trim();
    queue.push(
        {
            user: msgEventData.nick,
            args: args
        }
    );
    runQueue();
};

// See https://dev.streamelements.com/docs/widgets/6707a030af0b9-custom-widget-events#on-event
window.addEventListener('onEventReceived', function (obj) {
    if (obj.detail.listener !== "message") return;
    handleMessage(obj);
});

// See https://dev.streamelements.com/docs/widgets/6707a030af0b9-custom-widget-events#on-widget-load
window.addEventListener('onWidgetLoad', function (obj) {
    config = obj.detail.fieldData;
});